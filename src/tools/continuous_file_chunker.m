function continuous_chunks = continuous_file_chunker(input_data)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

delta_trig_time = diff([input_data.triggerTime]);

% find instances where triggerTime difference is greater than 12
separation_indices = find(delta_trig_time > 12);

continuous_chunks = {};

if isempty(separation_indices)
    continuous_chunks = input_data;
    return;
else
    separation_indices(end+1) = length(input_data);
    
    numcells = length(separation_indices);
    continuous_chunks = cell(1, numcells);
    
    ind = 1;
    for i=1:numcells
        continuous_chunks{i} = input_data(ind:separation_indices(i));
        ind = separation_indices(i)+1;
    end
end
end

