classdef TemperatureRegression
    %TemperatureRegression Base class for earplug insertion and removal
    %detection
    %   Provides a set of methods for both insertion and removal detection
    %   via regression curve fitting and residual thresholding. Intended
    %   usage involves both an InsertReg and RemovalReg object working in
    %   conjunction. 
    % Both InsertReg and RemovalReg function via a sliding window
    % calculation in which a curve is fit to the data and the sum squared
    % error is calculated. Computed SSE is then compared to a threshold
    % value, which determines insertion or removal areas. These two sets of
    % areas are then stitched together to form sets of inserted areas. 
    % User specifiable properties:
    %        Threshold: SSE value to compare against
    %        WindowSizeSeconds: Size of sliding window for each calculation
    %        Coeff: Coefficients of the curve to fit against
    %        Stepsize: Number of indices to advance for each calculation.
    %           Calculating every point along the temperature data is 
    %            typically not required. 
    % Example Usage:
    % Note - ALWAYS decimate temperature data by 100 before running
    % temp_deci = decimate_data(temperature_in, DECIMATION_FACTOR, false);
    % insert_scanner = InsertReg();
    % remove_scanner = RemovalReg();
    % insert_low_var_chunks = insert_scanner.process(temp_deci);
    % remove_low_var_chunks = remove_scanner.process(temp_deci);
    % stitched_chunks = TemperatureRegression.stitch(insert_low_var_chunks, remove_low_var_chunks, temp_deci);
    
    properties
        Threshold % SSE Threshold value
        WindowSizeSeconds
        Coeff
        BaselineData
        Timestep
        Stepsize
        WindowSizeSamples
        ShowPlots
        DASHR_SAMPLE_RATE = 1000;
        DECIMATION_FACTOR = 100;
    end
    
    methods (Abstract = true)
        regression_function(obj, t)
    end
    
    methods
        function time_out = generate_timestep(obj)
            decimated_sample_rate = obj.DASHR_SAMPLE_RATE/obj.DECIMATION_FACTOR;
            time_out = (0:1/(decimated_sample_rate):obj.WindowSizeSeconds);
        end
        
        function data_out = generate_baseline(obj)
            data_out = obj.regression_function(obj.Timestep);
        end
        
        function segmented_variance = process(obj, data_in, varargin)
            sse = [];
            
            % need to check to make sure there's enough values to meet
            % min_window_size
            if((length(data_in)) < obj.WindowSizeSamples)
                return;
            end
            
            % calculate SSE for each step
            for i = 1:obj.Stepsize:(length(data_in)-obj.WindowSizeSamples)
                y = data_in(i:(i+obj.WindowSizeSamples));
                yhat = obj.correct_offset(obj.BaselineData, y(1))';
                sse = [sse; i, obj.calculate_sse(y, yhat)];
            end
            
            % find areas below threshold
            threshold_areas = sse(find(sse(:,2) < obj.Threshold),:);
            
            % segment low variance areas into chunks, removing contiguous
            % sections
            segmented_variance = obj.segment_low_variance_areas(threshold_areas);
            
            %if obj.ShowPlots
            %    obj.plot_segmented_areas(data_in, segmented_variance, sse);
            %end
        end
        
        function sse = calculate_sse(obj, y, yhat)
            r = y - yhat;
            sse = norm(r, 2)^2; 
        end
        
        function deltaT = correct_offset(obj, t, temp_t0)
            offset = t(1) - temp_t0;
            deltaT = t - offset;
        end
        
        function segmented_variance = segment_low_variance_areas(obj, low_variance)
            segmented_variance = {};
            
            if isempty(low_variance)
                return;
            end
            
            continuous = diff(low_variance(:,1)) < (obj.Stepsize + 1);
            breaks = find(continuous == 0);
            
            if ~isempty(breaks)
                if(breaks(end) ~= length(low_variance))
                    breaks(end+1) = length(low_variance);
                end
                
                numcells = length(breaks);
                segmented_variance = cell(1, numcells);
                
                ind = 1;
                for i=1:numcells
                    segmented_variance{i} = low_variance(ind:breaks(i),:);
                    ind = breaks(i)+1;
                end
            else
                segmented_variance{1} = low_variance;
            end
        end
        
        function plot_segmented_areas(obj, data_in, segmented_variance, sse)
            figure;
            
            time_deci = (1:length(data_in))./(obj.DASHR_SAMPLE_RATE/obj.DECIMATION_FACTOR);
            ax1 = subplot(2,1,1);
            plot(time_deci,data_in);
            title('Temperature + Detected Areas');
            hold on
            for i = 1:length(segmented_variance)
                plot(time_deci(segmented_variance{i}(:,1):segmented_variance{i}(:,1)+obj.WindowSizeSamples), data_in(segmented_variance{i}(:,1):segmented_variance{i}(:,1)+obj.WindowSizeSamples),...
                    'LineWidth', 3, 'Color','m');
            end
            xlabel('Time (s)');
            ylabel('Temperature (DegC)');
            hold off
            
            ax2 = subplot(2,1,2);
            plot(time_deci(sse(:,1)), sse(:,2));
            title('Residuals');
            hold on
            for i = 1:length(segmented_variance)
                plot(time_deci(segmented_variance{i}(:,1)), segmented_variance{i}(:,2),...
                    '.','MarkerSize', 10, 'Color','m');
            end
            xlabel('Time (s)');
            ylabel('Residuals');
            hold off
            
            linkaxes([ax1, ax2],'x');
        end
    end
    
    methods (Static)
        function stitched_sections = stitch(insert_chunks, remove_chunks, temp_data)
            stitched_sections = {};
            
            % make sure both chunks have data, otherwise we have either 0 or 1 stitches
            if(isempty(insert_chunks))
                return;
            else
                stitched_sections{1}(1) = insert_chunks{1}(1);
            end
            
            if(isempty(remove_chunks))
                stitched_sections{1}(1) = insert_chunks{1}(1);
                stitched_sections{1}(2) = length(temp_data);
                return;
            end
            
            % pulls the first index from each separate insert chunk
            % we dont need every index from each chunk, just the first
            insert_index = [];
            for i = 1:length(insert_chunks)
                insert_index(i) = insert_chunks{i}(1);
            end
            
            % pulls the last index from each separate removal chunk
            % we dont need every index from each chunk, just the last
            remove_index = [];
            for i = 1:length(remove_chunks)
                %remove_index(i) = remove_chunks{i}(end,1);
                remove_index(i) = remove_chunks{i}(1);
            end
            
            stitched_array(1) = insert_index(1);
            %curr_target_index = stitched_array(1);
            curr_source_array = remove_index;
            curr_source_type = 'Remove';
            while(1)
                % find first index in source_array that is greater than target_index
                index_to_stitch = find(curr_source_array > stitched_array(end), 1, 'first');
                
                % if it's empty, we exhausted our list of remove indices, so leave
                % loop.
                if(~isempty(index_to_stitch))
                    stitched_array(end+1) = curr_source_array(index_to_stitch);
                    % swap source array
                    if strcmp(curr_source_type,'Remove')
                        curr_source_array = insert_index;
                        curr_source_type = 'Insert';
                    else
                        curr_source_array = remove_index;
                        curr_source_type = 'Remove';
                    end
                else
                    % break loop, we've exhausted our list of indices
                    
                    % if it's odd, we don't have a final ending remove, add it
                    if(mod(length(stitched_array), 2) ~= 0)
                        % TWO OPTIONS, 1) make NaN, 2) make length(temp_data)
                        stitched_array(end+1) = length(temp_data);
                    end
                    
                    % reshape into pairs
                    temp = reshape(stitched_array', [], length(stitched_array)/2)';
                    
                    % change to cell
                    stitched_sections = num2cell(temp, 2);
                    return;
                end
            end
        end
        
        function plot_stitched_sections(time, temp, stitched_chunks)
            figure;
            plot(time, temp)
            title('In-Ear Temperature Portions of Whole Data Set');
            xlabel('Time (s)');
            ylabel('Temperature (C)');
            hold on;
            for i = 1:length(stitched_chunks)
                plot(time(stitched_chunks{i}(1):stitched_chunks{i}(2)), temp(stitched_chunks{i}(1):stitched_chunks{i}(2)),...
                    'LineWidth', 3, 'Color','g');
            end
            hold off;
        end
    end
    
end

