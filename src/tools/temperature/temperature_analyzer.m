function stitched_chunks = temperature_analyzer(temperature_in, yesplot)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

DECIMATION_FACTOR = 100;

FLAG_USE_ABSOLUTE_THRESHOLD = true;
ABSOLUTE_THRESHOLD_C = 30;

% decimate and filter temperature data
temp_deci = decimate_data(temperature_in, DECIMATION_FACTOR, false);
time_deci = decimate_data((1:length(temperature_in))./1000, DECIMATION_FACTOR, false)';

%putvar(temp_deci);
%putvar(time_deci);

insert_scanner = InsertReg('ShowPlots',true);
remove_scanner = RemovalReg('ShowPlots',true);

insert_low_var_chunks = insert_scanner.process(temp_deci);
remove_low_var_chunks = remove_scanner.process(temp_deci);
stitched_chunks = TemperatureRegression.stitch(insert_low_var_chunks, remove_low_var_chunks, temp_deci);
if yesplot == 1
    TemperatureRegression.plot_stitched_sections(time_deci, temp_deci, stitched_chunks)
end

% note, need to rescale these chunks to the original temperature data, not
% the decimated. 
%putvar(insert_low_var_chunks);
%putvar(remove_low_var_chunks);

% need to rescale by the decimation factor
stitched_chunks = cellfun(@(x) x*DECIMATION_FACTOR, stitched_chunks,'UniformOutput',false);

%putvar(stitched_chunks);
end

function deltaT = insertion_temp_slow(t)
    % data from insert7_slow 
    deltaT = 37.07*exp(3.7e-05*t) + -1.027*exp(-.01645*t);

end

function deltaT = insertion_temp_fast(t)
    % data from insert7_fast
    deltaT = 33.97*exp(.0006064*t) + -6.054*exp(-.05286*t);
end

function deci_data = decimate_data(data_in, DECIMATION_FACTOR, output_to_workspace)
    deci_data = decimate(data_in, DECIMATION_FACTOR);
    if output_to_workspace
        putvar(deci_data);
    end
end




