classdef RemovalReg < TemperatureRegression
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties

    end
    
    methods
        function obj = RemovalReg(varargin)
            
            % set default values
            obj.Threshold = 400;
            obj.WindowSizeSeconds = 30;
            obj.Coeff = [-.076];
            obj.Stepsize = 10;
            obj.ShowPlots = false;
            
            % load values from varargin, will overwrite defaults as needed.
            for k = 1:2:length(varargin)
                obj.(varargin{k}) = varargin{k+1};
            end
            
            % calculated values
            obj.WindowSizeSamples = (obj.DASHR_SAMPLE_RATE/obj.DECIMATION_FACTOR)*obj.WindowSizeSeconds;
            obj.Timestep = obj.generate_timestep();
            obj.BaselineData = obj.generate_baseline();
        end
        
        function baseline_out = regression_function(obj, t)
            baseline_out = obj.Coeff(1) * t;
        end
        
    end
    
end

