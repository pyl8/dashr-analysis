classdef InsertReg < TemperatureRegression
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties 

    end
    
    methods
        function obj = InsertReg(varargin)
            
            % set default values
            obj.Threshold = 200;
            obj.WindowSizeSeconds = 20;
            obj.Coeff = [38.53, -0.0002443, -8.036, -.02458];
            obj.Stepsize = 10;
            obj.ShowPlots = false;
            
            % load values from varargin, will overwrite defaults as needed.
            for k = 1:2:length(varargin)
                obj.(varargin{k}) = varargin{k+1};
            end
            
            % calculated values
            obj.WindowSizeSamples = (obj.DASHR_SAMPLE_RATE/obj.DECIMATION_FACTOR)*obj.WindowSizeSeconds;
            obj.Timestep = obj.generate_timestep();
            obj.BaselineData = obj.generate_baseline();
        end
  
        function baseline_out = regression_function(obj, t)
               baseline_out = obj.Coeff(1)*exp(obj.Coeff(2)*t) + obj.Coeff(3)*exp(obj.Coeff(4)*t); 
        end
    end
    
end

