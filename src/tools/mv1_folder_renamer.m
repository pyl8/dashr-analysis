% Assists with renaming MV1_Downloader generated folders with DASHR serial
% number to the study participant ID. 

% Input variables - 
% downloadFolderPath - path to MV1 downloader main folder.
% pinPath - Path to a .csv file containing the serial number to PIN list

function [success] = mv1_folder_renamer(downloadFolderPath, pinPath)

% load sn to pin list
if exist(pinPath,'file') == 0
    disp('Error, pin list not found, exiting');
    success = false;
    return;
else
    pinList = csvread(pinPath);
end

% get list of folders in downloadFolderPath
files = dir(downloadFolderPath);
files(1:2) = [];
dirflags = [files.isdir];
folders = files(dirflags);

% find folders that should be renamed
% Serial numbers are always 10 digits including leading 0
CONST_SERIAL_LENGTH = 10;
folder_rename_flag = logical(zeros(1, length(folders)));
% increment through folders, checking if the length of the folder name ==
% const_serial_length (10), and that the first 3 characters are 026, which
% indicates that the folder represents a DASHR serial number and should be
% prepended with the study ID
for i = 1:length(folders)
    if(length(folders(i).name) == CONST_SERIAL_LENGTH && strcmp(folders(i).name(1:3),'026') == 1)
        folder_rename_flag(i) = 1;
    end
end

folders_to_rename = folders(folder_rename_flag);

% check list of folders against list of registered pins
for i = 1:length(folders_to_rename)
    [res, pin_to_prepend] = pinLookup(pinList, folders_to_rename(i).name);
    if res == true
        movefile(fullfile(downloadFolderPath,folders_to_rename(i).name), fullfile(downloadFolderPath, strcat(pin_to_prepend,'-',folders_to_rename(i).name)));
    end
end

success = true;
return;
end

function [result, PIN] = pinLookup(pinList, serialNumber)
    result = false;
    PIN = '';
    
    serial_index = find(pinList == str2num(serialNumber));

    if(serial_index > 0)
        PIN = num2str(pinList(serial_index(1), 2));
        result = true;
    else
        return;
    end
end




