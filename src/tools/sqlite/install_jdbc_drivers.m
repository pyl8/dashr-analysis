function status = install_jdbc_drivers()
%INSTALL_JDBC_DRIVERS Installs the requisite JDBC sqlite database driver
%   See https://www.mathworks.com/help/database/ug/sqlite-jdbc-windows.html
%   Returns true if javaclasspath.txt was updated, false otherwise.
%   REQUIRES MATLAB RESTART AFTER FUNCTION COMPLETION

matlab_dir_path = prefdir();
filename = 'javaclasspath.txt';
jdbc_driver_name = 'sqlite-jdbc-3.16.1.jar';

jdbc_path = fullfile(fileparts(which('install_jdbc_drivers.m')), jdbc_driver_name);

status = false;

% check to make sure MATLAB2016b is installed, otherwise just quit because
% it's not compatible
if verLessThan('matlab','9.1')
    error('Error: MATLAB R2016a or greater required.');
    return;
end


% open or create file, append contents if already exists
fid = fopen(fullfile(matlab_dir_path, filename), 'a+');

% read the contents to check if the path is already there
% rewind first, since we opened in '+' mode
fseek(fid, 0, 'bof');
contents = fread(fid, 'char');

% search contents for the database driver
% if it's not found, insert
if isempty(strfind(contents', jdbc_path))
    fseek(fid, 0, 'eof');
    fwrite(fid, [jdbc_path, 13,10]);
    status = true;
end

% close file
fclose(fid);

% restart matlab if changes were made to javaclasspath.txt
if status
    if strcmp(displayRestartPrompt(), 'OK')
        system('matlab &');quit
    else
        disp('MATLAB requires restart before program can continue');
    end
end
end

function choice = displayRestartPrompt()
    options.Interpreter = 'tex';
    qString = 'MATLAB requires restart for changes to take effect. Restart will take place after clicking ''OK''';
    options.Default = 'OK';
    choice = questdlg(qString, 'Restart?',...
        'OK','Cancel', options);
end

