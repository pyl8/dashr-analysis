function status = create_sqlite_db(path)
%CREATE_SQLITE_DB Creates an sqlite database file, if path does not already
%   exist
%   Returns false if database already exists, or if database was not able
%   to be created.
%   Returns true if database was successfully created
%   Reference : https://www.mathworks.com/help/database/ug/sqlite.html

status = false;
if exist(path, 'file')
    return;
else
    try
        conn = sqlite(path, 'create');
        close(conn);
        status = true;
    catch ME
        disp('Error creating sqlite database');
        disp(ME);
        status = false;
    end
end

end

