function [success] = mv1_modified_date_restorer(pathToFile)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

success = false;

% make sure file exists
if(exist(pathToFile, 'file') == 0)
    return;
end

% should just read the header information in, not the whole file
fid = fopen(fullfile(pathToFile));

% read a 512 byte block
[data, count] = fread(fid, 512,'uint8=>uint8');
fclose(fid);

if ~isempty(data)
    if count == 512 && data(1) == 99
        triggerTime = typecast(data(41:44), 'int32');
        setFileDate(pathToFile, triggerTime);
        success = true;
    end
end



end

function setFileDate(fn,date)
    % date needs to be UTC, milliseconds since 1/1/1970
    % our trigger time is in seconds, so convert to double to prevent
    % overflow and *1000.
    java.io.File(fn).setLastModified(double(date)*1000);
end


