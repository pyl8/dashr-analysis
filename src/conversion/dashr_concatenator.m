% % Script that takes two .mat files, one with data from someone getting out 
% % of a football stance, and one with data from a drop test.
% % The script asks the user for the start time of the stance data and the
% % two seconds to extract to act as "pre-impact" data. The script then 
% % concatenates the 2000ms subset of the stance data with the impact data
% % (10ms prior and 90ms after the peak linear acceleration) by interpolating
% % the two datasets. Since there is no set time after leaving the stance
% % that a player typically experiences an impact, several severities of 
% % interpolations are performed for each concatenation. 
% 
% clear;
% close all; 
% 
% %% User selection of two .mat files
% stance_file = uigetfile('*.mat', 'Select file with stance data');
% impact_file = uigetfile('*.mat', 'Select file with impact data');
% 
% %% Ask the user for the start time of the stance data in HH:MM:SS format
% % Also get the 2-second timeframe to extract
% stance_start_time_string = input('Enter start time for stance data: ', 's');
% stance_start_time_ms = getTimeMS(stance_start_time_string);
% stance_extract_time_string = input('Enter start time for 2000ms subset: ', 's');
% stance_extract_time_ms = getTimeMS(stance_extract_time_string);
% 
% %% Load .mat files
% load(stance_file, 'raw_data');
% stance_lin_acc = raw_data.lin_acc_resultant;
% stance_rot_vel = raw_data.rot_vel_resultant;
% 
% load(impact_file, 'lin_acc_res_subset', 'rot_vel_res_subset');
% impact_lin_acc = lin_acc_res_subset;
% impact_rot_vel = rot_vel_res_subset;
% 
% %% Filter vectors with 4th order Butterworth filter, Fc = 500 Hz
% fc = 500;
% fs = 1000;
% 
% % [b, a] = butter(4,fc/(fs/2));
% % stance_lin_acc_filtered = filter(b, a, stance_lin_acc);
% % stance_rot_vel_filtered = filter(b, a, stance_rot_vel);
% % impact_lin_acc_filtered = filter(b, a, impact_lin_acc);
% % impact_rot_vel_filtered = filter(b, a, impact_rot_vel);
% 
% %% Extract a 2000ms subset of the stance data, using the given times
% stance_start_index = stance_extract_time_ms - stance_start_time_ms + 1;
% stance_lin_acc_subset = stance_lin_acc(stance_start_index:stance_start_index + 1999);
% stance_rot_vel_subset = stance_rot_vel(stance_start_index:stance_start_index + 1999);
% 
% %% Extract a 100ms subset of the impact data, 10ms prior and 90ms after impact
% impact_start_index = length(impact_lin_acc) - 110;
% impact_lin_acc_subset = impact_lin_acc(impact_start_index:impact_start_index + 99);
% impact_rot_vel_subset = impact_rot_vel(impact_start_index:impact_start_index + 99);

%% Plot separate components
curFig = figure('units','normalized','outerposition',[0 0 1 1]);%,'visible','off');
clf;
graph2(1) = subplot(2,1,1);
time_vals = 1:1:length(stance_lin_acc_subset);
plot(time_vals, stance_lin_acc_subset);
xlabel('\fontsize{16}Time (ms)');
ylabel('\fontsize{16}Linear Acceleration (g)');
title('Linear acceleration'); 

% Rotational Velocity
graph2(2) = subplot(2,1,2);
plot(time_vals, stance_rot_vel_subset);
xlabel('\fontsize{16}Time (ms)');
ylabel('\fontsize{16}Rotational Velocity (deg/s)');
title('Rotational Velocity');
fulltitle = "Stance subset of data pre-interpolation";
sgtitle(fulltitle,'Interpreter','none');

linkaxes(graph2,'x')

curFig = figure('units','normalized','outerposition',[0 0 1 1]);%,'visible','off');
clf;
graph2(1) = subplot(2,1,1);
time_vals = 1:1:length(impact_lin_acc_subset);
plot(time_vals, impact_lin_acc_subset);
xlabel('\fontsize{16}Time (ms)');
ylabel('\fontsize{16}Linear Acceleration (g)');
title('Linear acceleration'); 

% Rotational Velocity
graph2(2) = subplot(2,1,2);
plot(time_vals, impact_rot_vel_subset);
xlabel('\fontsize{16}Time (ms)');
ylabel('\fontsize{16}Rotational Velocity (deg/s)');
title('Rotational Velocity');
fulltitle = "Impact subset of data pre-interpolation";
sgtitle(fulltitle,'Interpreter','none');

linkaxes(graph2,'x')

%% Concatenate the two arrays of linear acceleration and rotational velocity 
% Also save these arrays in a folder with clear names 
folder_name = 'training_set';
if ~isfolder(folder_name)
    mkdir(folder_name);
end
%cd(folder_name);
interp_type = 'makima';

for i = 50
%     concat_lin_acc = [stance_lin_acc_subset(i+1:length(stance_lin_acc_subset)) ; impact_lin_acc_subset];
%     concat_interp_vals_full = 1:1:(length(stance_lin_acc_subset) + length(impact_lin_acc_subset));
%     concat_interp_vals = [1:1:2000-i 2001:2100];
%     concat_lin_acc_interp = interp1(concat_interp_vals, concat_lin_acc, concat_interp_vals_full, interp_type);
%     concat_rot_vel = [stance_rot_vel_subset(i+1:length(stance_rot_vel_subset)) ; impact_rot_vel_subset];
%     concat_rot_vel_interp = interp1(concat_interp_vals, concat_rot_vel, concat_interp_vals_full, interp_type);

    concat_lin_acc = [stance_lin_acc_subset(i+1:length(stance_lin_acc_subset)) ; NaN(i,1) ; impact_lin_acc_subset];
    concat_lin_acc_nogaps = fillgaps(concat_lin_acc, i, 50);
    concat_rot_vel = [stance_rot_vel_subset(i+1:length(stance_rot_vel_subset)) ; NaN(i,1) ; impact_rot_vel_subset];
    concat_rot_vel_nogaps = fillgaps(concat_rot_vel, i, 50);
    
    curFig = figure('units','normalized','outerposition',[0 0 1 1]);%,'visible','off');
    clf;
    graph2(1) = subplot(2,1,1);
    %time_vals = 1:1:length(concat_lin_acc_interp);
    time_vals = 1:1:length(concat_lin_acc_nogaps);
    plot(time_vals, concat_lin_acc_nogaps);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration (g)');
    title(strcat('Linear Acceleration Interpolated ', int2str(i))); 

    % Rotational Velocity
    graph2(2) = subplot(2,1,2);
    plot(time_vals, concat_rot_vel_nogaps);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Rotational Velocity (deg/s)');
    title('Rotational Velocity');
    fulltitle = "Interpolation of " + int2str(i);
    sgtitle(fulltitle,'Interpreter','none');

    linkaxes(graph2,'x')
end



% convert time in HH:MM:SS format to ms 
function time_ms = getTimeMS(time_string)
    time_ms_array = strsplit(time_string, ':');
    time_ms = (str2double(cell2mat(time_ms_array(1))).*36e5) + ...
        (str2double(cell2mat(time_ms_array(2))).*6e4) + ...
        (str2double(cell2mat(time_ms_array(3))).*1000);
end
