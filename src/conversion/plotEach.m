% Very rough script to load and plot binned data from DASHR
% Each figure gets loaded, plotted, saved to a .fig and stored in a folder


prompt = {'Enter PIN Number:'};
promptTitle = 'Input';
dims = [1 35];
answer = inputdlg(prompt,promptTitle,dims)
%%
load(sprintf('binned%4s.mat', char(answer)));
%%
saveFig = true
savePNG = true
if ~isdir(sprintf('%4sFigs', char(answer)))
    mkdir(sprintf('%4sFigs', char(answer)));
end
if ~isdir(sprintf('%4sPNGs', char(answer)))
    mkdir(sprintf('%4sPNGs', char(answer)));
end

curFig = figure(1)

plot(time, lin_acc_resultant,...
    time, lin_acc_xyz(:,1),...
    time, lin_acc_xyz(:,2),...
    time, lin_acc_xyz(:,3));
xlabel('\fontsize{16}Time (ms)');
ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
%legend('Resultant','x dir','y dir','z dir');
legend('resultant',  'x dir','y dir','z dir');
title('\fontsize{18}Linear Acceleration (x,y,z) Full');


if savePNG
    saveas(curFig, sprintf('%4sPNGs/FullRun.png', char(answer)))
end
if saveFig
    savefig(curFig, sprintf('%4sFigs/FullRun.fig', char(answer)))
end
%%
for bin = 1:14
    %Plot the resultant over time ONLY
    %figure(1)
    curFig = figure(1)
    plot(newTime(bin, :), newlin_acc_resultant(bin,:))
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    title(['\fontsize{18}' sprintf('Linear Acceleration Resultant, Bin %d', bin)]);
    
    if saveFig
        savefig(curFig, sprintf('%4sFigs/Bin%02d-Res.fig', char(answer), bin))
    end
    if savePNG
        saveas(curFig, sprintf('%4sPNGs/Bin%02d-Res.png', char(answer), bin))
    end
    
    %Plot the three components, same plot
    curFig = figure(2)
    %figure('units','normalized','outerposition',[0 0 1 1]); %why did I do this?
    plot(newTime(bin, :), newlin_acc_x(bin,:),...
        newTime(bin, :), newlin_acc_y(bin,:),...
        newTime(bin, :), newlin_acc_z(bin,:));
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    %legend('Resultant','x dir','y dir','z dir');
    legend('x dir','y dir','z dir');
    title(['\fontsize{18}' sprintf('Linear Acceleration (x,y,z), Bin %d', bin)]); 
    
    
    if saveFig
        savefig(curFig, sprintf('%4sFigs/Bin%02d-XYZComp.fig', char(answer), bin))
    end
    if savePNG
        saveas(curFig, sprintf('%4sPNGs/Bin%02d-XYZComp.png', char(answer), bin))
    end
    %Plot the three components and resultant, separate plots
    
    curFig = figure(3)
    graph1(1) = subplot(2,1,1)
    
    plot(newTime(bin, :), newlin_acc_x(bin,:),...
        newTime(bin, :), newlin_acc_y(bin,:),...
        newTime(bin, :), newlin_acc_z(bin,:));
    legend('x dir','y dir','z dir');
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    title(['\fontsize{18}' sprintf('Linear Acceleration (x,y,z), Bin %d', bin)]);
    
    graph1(2) = subplot(2,1,2)
    plot(newTime(bin, :), newlin_acc_resultant(bin,:))
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    legend('resultant');
    title(['\fontsize{18}' sprintf('Linear Acceleration Resultant, Bin %d', bin)]);
    
    linkaxes([graph1],'x')
    
    if saveFig
        savefig(curFig, sprintf('%4sFigs/Bin%02d-CompRes.fig', char(answer), bin))
    end
    if savePNG
        saveas(curFig, sprintf('%4sPNGs/Bin%02d-CompRes.png', char(answer), bin))
    end    
    
   
end