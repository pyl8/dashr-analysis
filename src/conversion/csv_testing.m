a = [1 2 3];
b = [4 5 6];
a_csv = cat(2,"Time",a);
b_csv = cat(2,"Linear Acceleration",b);
final_csv = [a_csv ; b_csv];

fileID_csv = 'test.csv';
writematrix(final_csv, fileID_csv);
%writematrix("\n", fileID_csv);
%writematrix(b, fileID_csv);