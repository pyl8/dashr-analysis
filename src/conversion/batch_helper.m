function [output,metadata] = batch_helper(varargin)
% arg 1 = should be relative path to output directory.
% arg 1 needs to be implemented properly. currently not using output
% directories for any analysis.


% default to DASHR type
type = 'DASHR';
if isempty(varargin)
    flag_createDirectory = false;
else
    flag_createDirectory = varargin{1};
end

if length(varargin) > 1
    type = varargin{2};
end



% Load the configuration file, if it exists
if exist('batchhelper_configuration.mat', 'file')
    try 
        load batchhelper_configuration.mat
    catch 
        disp('Error loading configuration file');
    end
else
    % set up default variables
    lastFolder = pwd;
end

[filenames, pathname] = uigetfile({'*.BIN';'*.CSV'},'Multiselect','on','Choose DASHR files',lastFolder);

output_directory = '';
if flag_createDirectory == true
    output_directory = [pathname, 'Converted'];
    mkdir(output_directory);
end

% single file selection for uigetfile returns a char array, so convert it
% to a cell for easier processing later
if ~iscell(filenames)
    filenames = {filenames};
end

output = [];
metadata = [];

if ~isempty(filenames) && isequal(filenames{1},0) ~= 1
    % update configuration file with most recent folder
    lastFolder = pathname;
    try
        save('batchhelper_configuration.mat','lastFolder');
    catch
        disp('Error saving configuration file');
    end
    
    h = waitbar(0,'Loading files');
    count = 1;
    for i=filenames
        [output_temp, meta_temp] = bin_reader(pathname, char(i), type);
        if flag_createDirectory
            file_writer(meta_temp, output_temp, output_directory, 'DASHR', 'CSV');
        end
        
        output = [output; output_temp];
        metadata = [metadata; meta_temp];
        waitbar(count/length(filenames), h);
        count = count + 1;
    end
    delete(h);
else
    msgbox('No files selected for conversion. Please rerun file selection.','File Select Error','warn');
end

end

