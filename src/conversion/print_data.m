function print_data(data)
    count = 1;
    sizevec = size(data);
    
    while count < sizevec(1)
        if mod(count, 15) == 0
            fprintf('%4d\n', data(count));
        else
            fprintf('%4d ', data(count));
        end
        count = count + 1;
    end
    fprintf('%4d\n', data(sizevec(1)));

end