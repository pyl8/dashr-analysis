function isolate_impacts(varargin)

if isfile('isolate_impacts.m') ~= 1
    error('Wrong folder');
end

if isempty(varargin)
    impact_recorder();
    return
end

pin = varargin{1};
date = varargin{2};
start_time = varargin{3};
LAT = varargin{4};
DT = varargin{5};
plot_fig = varargin{6};
temp_verif = varargin{7};
light_verif = varargin{8};
output = evalin('base', 'output');

topFolder = pwd;
foldername = makefolder(pin, date);
rs = convert_data(output); % raw data stored in raw_struct (rs)
if plot_fig == 1
    plotfig(rs); % plots full figure but does not save
end

stitched_chunks = temperature_analyzer(rs.temp_raw, 0);
RoI = 0.15; % percentage threshold for adding to duration
RLT = 75000; % light level threshold, placeholder that will almost always return true
cd(foldername); % move into that day's folder
impfoldername = strcat(pin, '_', date, '_LAT', int2str(LAT), '_DT', int2str(DT));

if ~isfolder(impfoldername) % thresholds have not been analyzed before
    mkdir(impfoldername)
else % thresholds have been analyzed previously, override folder
    rmdir(impfoldername, 's');
    mkdir(impfoldername);
end

start_ms = get_start_ms(start_time);

% text file w/ descriptions of each impact
fileID = fopen('impactlog.txt', 'w'); 
% excel file w/ descriptions of each impact
fileID_xls = strcat('impactlog_',foldername,'_LAT',int2str(LAT),'_DT',int2str(DT),'.xlsm');

i = 1;
count = 1;
yesimpact = 0;

while (i < length(rs.time)*0.9) % loops through the entire dataset
    k = i;
    if (rs.lin_acc_resultant(i) > LAT && rs.lin_acc_resultant(i) < 300) % potential impact, 2nd condition removes corrupted data
        peaklinacc = rs.lin_acc_resultant(i);
        duration = 0;
        k = i+1;
        upperbound = i+100;
        while (k < upperbound && k < length(rs.time))
            upperbound = i+100;
            if (rs.lin_acc_resultant(k) > peaklinacc) % higher peak 
                peaklinacc = rs.lin_acc_resultant(k);
                i = k;
                duration = 0;
            elseif (rs.lin_acc_resultant(k) > RoI*peaklinacc) % part of impact
                duration = duration + 1;
            else
                break;
            end
            k = k+1;
        end
        j = i-1;
        while (j > i-40 && j > 1) % work backwards to start of impact
            if (rs.lin_acc_resultant(j) > RoI*peaklinacc) 
                duration = duration + 1;
            else
                break;
            end
            j = j-1;
        end
        
        if (rs.light_ref(i) <= RLT) % meets light threshold
            lightIn = true;
            j = j+1;
            while (j < k) % check whether it meets threshold for long enough
                if (rs.light_ref(j) > RLT)
                    lightIn = false;
                    break;
                end
                j = j+1;
            end
        end
        
        tempIn = tempcheck(i, stitched_chunks, temp_verif);
        if (temp_verif == 1) 
            if (tempIn)
                temp_string = "suggests in ear";
            else
                temp_string = "suggests not in ear";
            end
        else
            temp_string = "not checked";
        end
        
        if (light_verif == 1)
            if (lightIn)
                light_string = "suggests in ear";
            else
                light_string = "suggests not in ear";
            end
        else
            light_string = "not checked";
        end
        
        if (duration >= DT && tempIn) % meets duration threshold
            yesimpact = 1;
            impact_time_ms = i + start_ms;
            % prints info to console
            fprintf('%d: Impact at %0.2d:%0.2d:%0.2d, Peak linacc = %0.1f g, Duration = %d ms, Temperature %s, Light %s\n', ...
            count, floor(impact_time_ms/36e5), floor((mod(impact_time_ms,36e5))/6e4), ...
            floor(mod(mod(impact_time_ms,36e5),6e4)/1000),...
            peaklinacc, duration, temp_string, light_string); 
            imp_time = strcat(num2str(floor(impact_time_ms/36e5), '%0.2d'), ':', ...
                num2str(floor((mod(impact_time_ms,36e5))/6e4), '%0.2d'), ':', ...
                num2str(floor(mod(mod(impact_time_ms,36e5),6e4)/1000), '%0.2d'));
            imp_time_str = string(imp_time);
            
            % prints info to impact log
            fprintf(fileID, '%d: Impact at %0.2d:%0.2d:%0.2d, Peak linacc = %0.1f g, Duration = %d ms, Temperature %s, Light %s\n', ...
                count, floor(impact_time_ms/36e5), floor((mod(impact_time_ms,36e5))/6e4), ...
                floor(mod(mod(impact_time_ms,36e5),6e4)/1000),...
                peaklinacc, duration, temp_string, light_string);

            % saves info to excel file
            if count == 1
                writematrix("Impact Number", fileID_xls, 'Range', 'A1');
                writematrix("Time", fileID_xls, 'Range', 'B1');
                writematrix("Peak (g)", fileID_xls, 'Range', 'C1');
                writematrix("Duration (ms)", fileID_xls, 'Range', 'D1');
                writematrix("Temperature", fileID_xls, 'Range', 'E1');
                writematrix("Light", fileID_xls, 'Range', 'F1');
            end
            writematrix(count, fileID_xls, 'Range', strcat('A', int2str(count+1)));
            writematrix(imp_time_str, ...
                fileID_xls, 'Range', strcat('B', int2str(count+1)));
            writematrix(peaklinacc, fileID_xls, 'Range', strcat('C', int2str(count+1)));
            writematrix(duration, fileID_xls, 'Range', strcat('D', int2str(count+1)));
            writematrix(temp_string, fileID_xls, 'Range', strcat('E', int2str(count+1)));
            writematrix(light_string, fileID_xls, 'Range', strcat('F', int2str(count+1)));

            % creates zoomed in figure
            curFig = figure('units','normalized','outerposition',[0 0 1 1],'visible','off');
            clf;
            graph2(1) = subplot(2,1,1);
            hold on;
            time_vals = linspace(-40,100,141);
            plot(time_vals, rs.lin_acc_resultant(i-40:i+100),...
                time_vals, rs.lin_acc_xyz(i-40:i+100,1),...
                time_vals, rs.lin_acc_xyz(i-40:i+100,2),...
                time_vals, rs.lin_acc_xyz(i-40:i+100,3));
            xlabel('\fontsize{16}Time (ms)');
            ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
            legend('Resultant','x dir','y dir','z dir');
            legend('Resultant','x dir','y dir','z dir');
            title('Linear Acceleration (x,y,z and resultant)');
            hold off 

            % Rotational Velocity
            graph2(2) = subplot(2,1,2);
            hold on;
            plot(time_vals, rs.rot_vel_resultant(i-40:i+100),...
                time_vals, rs.rot_vel_xyz(i-40:i+100,1),...
                time_vals, rs.rot_vel_xyz(i-40:i+100,2),...
                time_vals, rs.rot_vel_xyz(i-40:i+100,3));
            xlabel('\fontsize{16}Time (ms)');
            ylabel('\fontsize{16}Rotational Velocity (deg/s)');
            legend('Resultant','x dir','y dir','z dir');
            title('Rotational Velocity (x,y,z and resultant)');
            hold off
            fulltitle = strcat(pin, ',', date, ',', imp_time);
            sgtitle(fulltitle,'Interpreter','none');

            linkaxes(graph2,'x')

            % saves figure and mat file to folder
            impfigname = strcat(pin, '_', date, '-', int2str(count), '.fig');
            impmatname = strcat(pin, '_', date, '-', int2str(count), '.mat');
            set(curFig, 'CreateFcn', 'set(gcf,"Visible","on")'); % enables opening of subplots later
            saveas(curFig,impfigname,'fig');
            movefile(impfigname, impfoldername);
            timesubset = rs.time(i-40:1+100);
            lin_acc_res_subset = rs.lin_acc_resultant(i-40:i+100);
            lin_acc_xyz_subset = rs.lin_acc_xyz(i-40:i+100,:);
            rot_vel_res_subset = rs.rot_vel_resultant(i-40:i+100);
            rot_vel_xyz_subset = rs.rot_vel_xyz(i-40:i+100,:);

            % saves necessary variables to mat file for further
            % analysis
            save(impmatname, 'date', 'peaklinacc', 'timesubset', 'lin_acc_res_subset', ...
                'lin_acc_xyz_subset', 'rot_vel_res_subset', ...
                'rot_vel_xyz_subset', 'start_time');
            movefile(impmatname, impfoldername)
            count = count + 1;
        end  
        k = k + 100;
    end
    i = k + 1; % avoids recording the same impact multiple times
end

movefile('impactlog.txt', impfoldername);
if yesimpact == 1
    movefile(fileID_xls, impfoldername);
end
cd(topFolder); % return to original folder

end

%% User input, folder detection/creation
function foldername = makefolder(pin, date)
    foldername = strcat(pin, '_', date);
    if ~isfolder(foldername) % first time analyzing a set of data
        mkdir(foldername);
    end
end

%% Convert to raw data
function raw_struct = convert_data(output)
    num_milsec_total = length(output) * length(output(1).data);
    raw_data = zeros(num_milsec_total,9);
    start_index = 1;
    end_index = length(output(1).data);
    for i = 1:length(output)
        raw_data(start_index:end_index,:) = output(i).data;
        start_index = end_index + 1;
        if i < length(output)
            end_index = start_index + length(output(i+1).data) - 1;
        end
    end

    %saves data in arrays easy to use for plotting
    raw_struct.lin_acc_xyz = raw_data(:,1:3); %units = g
    raw_struct.lin_acc_resultant = sqrt(raw_struct.lin_acc_xyz(:,1).^2 + ...
        raw_struct.lin_acc_xyz(:,2).^2 + raw_struct.lin_acc_xyz(:,3).^2);
    raw_struct.rot_vel_xyz = raw_data(:,4:6); %units = deg/s
    raw_struct.rot_vel_resultant = sqrt(raw_struct.rot_vel_xyz(:,1).^2 + ...
        raw_struct.rot_vel_xyz(:,2).^2 + raw_struct.rot_vel_xyz(:,3).^2);
    raw_struct.time = 1:1:length(raw_data); %units = ms
    raw_struct.temp_raw = raw_data(:,9);
    raw_struct.light_amb = raw_data(:,7);
    raw_struct.light_ref = raw_data(:,8);
end

%% Plot and save full figure
function plotfig(s)
    curFig = figure('units','normalized','outerposition',[0 0 1 1]); %why did I do this?
    clf
    graph1(1) = subplot(2,2,1);
    hold on;
    plot(s.time, s.lin_acc_resultant,...
        s.time, s.lin_acc_xyz(:,1),...
        s.time, s.lin_acc_xyz(:,2),...
        s.time, s.lin_acc_xyz(:,3));
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    legend('Resultant','x dir','y dir','z dir');
    title('\fontsize{24}Linear Acceleration (x,y,z and resultant)');
    hold off 

    % Rotational Velocity
    graph1(2) = subplot(2,2,2);
    hold on;
    plot(s.time, s.rot_vel_resultant,...
        s.time, s.rot_vel_xyz(:,1),...
        s.time, s.rot_vel_xyz(:,2),...
        s.time, s.rot_vel_xyz(:,3));
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Rotational Velocity (deg/s)');
    legend('Resultant','x dir','y dir','z dir');
    title('\fontsize{24}Rotational Velocity (x,y,z and resultant)')
    hold off

    % temperature
    graph1(3) = subplot(2,2,3);
    hold on;
    plot(s.time, s.temp_raw);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Temperature');
    title('\fontsize{24}Temperature');
    hold off

    % light
    graph1(4) = subplot(2,2,4);
    hold on;
    plot(s.time, s.light_amb, s.time, s.light_ref);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Light level');
    legend('Ambient light','Reflected light');
    title('\fontsize{24}Light');
    hold off

    linkaxes(graph1,'x') 
end

%% Collect start time
function start_ms = get_start_ms(start_time)
    strt = strsplit(start_time, ':');
    start_ms = (str2double(cell2mat(strt(1))).*36e5) + ...
        (str2double(cell2mat(strt(2))).*6e4) + ...
        (str2double(cell2mat(strt(3))).*1000); % converts to ms
end

function inRange = tempcheck(time_in, stitched_chunks, check_temp)
    inRange = false; 
    if check_temp == 0
        inRange = true;
    else
        for i = 1:length(stitched_chunks)
            if (time_in-5 >= stitched_chunks{i}(1) && time_in+5 <= stitched_chunks{i}(2))
                inRange = true;
                break
            end
        end
    end

end
