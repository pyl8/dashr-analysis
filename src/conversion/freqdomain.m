load 3580_2018-10-16_TUES-33.mat

SpecMag = abs(fft(lin_acc_res_subset));
delFreq = 1000/length(lin_acc_res_subset);

figure(1);
clf;
plot(0:delFreq:1000-delFreq, SpecMag);
xlabel('Frequency (Hz)');
ylabel('Magnitude Spectrum');