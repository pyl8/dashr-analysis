function HIC = duration_analysis(filename)
% duration_analysis 
% Loads a .mat file from a potential impact and calculates HIC and the 
% start/endpoints for that calculation 

load(filename);

linacc = lin_acc_res_subset;

% apply moving average filter 
a = 1;
b = [1/4 1/4 1/4 1/4];
linacc_filtered = filter(b, a, linacc);

duration_HIC40 = 0;
HIC = -1; 
i = 41;
HICinteg = cumtrapz(linacc(i-8:i+32));
startms = -1;
endms = -1;
intfactor = 2.5;

for ih = 33:73
    for jh = ih+1:min([ih+36,73])
        %tempHIC = (jh-ih)*(((jh-ih)^(-1) * (HICinteg(jh-32) - HICinteg(ih-32)))^intfactor);
        tempHIC = (jh-ih)^(-1) * ((HICinteg(jh-32) - HICinteg(ih-32))^intfactor);
        %fprintf('HIC from %d to %d: %0.2d\n', (ih-41), (jh-41), tempHIC);
        if (tempHIC > HIC)
            HIC = tempHIC;
            duration_HIC40 = jh-ih;
            startms = ih-41;
            endms = jh-41;
        end
    end
end

fprintf('Duration: %d ms, Start = %d ms, End = %d ms', duration_HIC40, ...
    startms, endms);

end

