% Patrick Liu
% Impact Recorder (originally data_analysis_robust)
% 4/2/19 1st draft 
% 4/22/19 2nd draft, saves fig, mat files, and impact log to folder
% 4/30/19 3rd draft, implemented suggestions from Dr. Luck and Mitchell
% 4/28/20 4th draft, more user-friendly and support for Excel output
% 6/26/20 5th draft, switched to impact_recorder app

% to do: clean up code, split into functions
% support exporting to csv for tensorflow
if isfile('impact_recorder_script.m') ~= 1
    error('Wrong folder');
end

impact_recorder();