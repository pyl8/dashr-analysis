% This function launches a GUI that allows the user to input a PIN, 
% linear acceleration threshold, duration threshold, and to enable/disable
% basic temperature/light checks. The function then goes through every 
% subfolder of the overall PIN folder, converting all binary files in each
% dataset to linear acceleration, rotational velocity, temperature, and
% light matrices. For each dataset, this function saves all potential
% impacts (2000ms prior to peak linear acceleration, 100ms after) in a .mat
% file, Excel spreadsheet, and .csv file. 
function dashr_analysis_pin(varargin)

if isfile('impact_recorder_pin.mlapp') ~= 1
    error('Wrong folder');
end

if isempty(varargin)
    impact_recorder_pin();
    return
end

origFolder = pwd; % save for later

pinFolder = uigetdir();
convertpath = fileparts(which('DASHR_Converter_pin.m')); % add path to DASHR Converter function
addpath(genpath(fullfile(convertpath)));
addpath(genpath(fullfile(convertpath,'../tools')));
addpath(genpath(fullfile(convertpath,'../display')));

conversionParameters.type = 'DASHR';
conversionParameters.generate_files = false;
conversionParameters.output_format = 'MATLAB';
conversionParameters.workspace = true;
conversionParameters.file_selection = 'Recursive';

pin = varargin{1};
LAT = varargin{2};
DT = varargin{3};
temp_verif = varargin{4};
light_verif = varargin{5};
plot_fig = varargin{6};

if ~isfolder(pin) % first time analyzing a set of data
    mkdir(pin);
end
cd(pin);

impfoldername = strcat(pin, '_LAT', int2str(LAT), '_DT', int2str(DT));
if ~isfolder(impfoldername) % thresholds have not been analyzed before
    mkdir(impfoldername)
else % thresholds have been analyzed previously, ask to override folder
    override = input('Enter Y to override existing directory:', 's');
    if (strcmp(override, 'Y')) % replace all files w/ new analysis
        rmdir(impfoldername, 's');
        mkdir(impfoldername);
    end
end

mainFolder = pwd; % used to return to pin folder

%% Loop through folders for each day and perform full data analysis 
f_full = dir(fullfile(pinFolder,'*'));
folders = setdiff({f_full([f_full.isdir]).name},{'.','..'}); % list of subfolders
for ii = 1:numel(folders)
   [~,day,~] = fileparts(folders{ii});
   try
       if (isfile(strcat(pin, '_', day, '_rawdata.mat')))
           load(strcat(pin, '_', day, '_rawdata.mat'), 'raw_data');
       else
           conversionParameters.files = rdir(fullfile(pinFolder,folders{ii}, '**','*.BIN'));
           output = DASHR_Converter_pin(conversionParameters);
           raw_data = convert_data(output); % store raw data in raw_struct (rs)
           rawdatafilename = strcat(pin, '_', day, '_rawdata.mat');
           save(rawdatafilename, 'raw_data'); % save in pin folder
       end
       cd(impfoldername);
       if (plot_fig == 1) 
           plotfig(raw_data); % plots full figure but does not save
       end
       isolate(raw_data, pin, LAT, DT, temp_verif, light_verif, day);
       cd(mainFolder);
   catch
       fprintf('Error converting or analyzing %s\n', day);
   end
end

cd(origFolder);

%% Isolate impacts for a dataset
function isolate(raw_data, pin, LAT, DT, temp_verif, light_verif, day)
impFolder = pwd; % save for later 

% file names for saving  overall information for a given PIN 
fileID_xls = strcat('impactlog_', pin, '_LAT', int2str(LAT), '_DT', int2str(DT), '.xlsm');

day_folder = makefolder(pin, day);
if ~isfolder(day_folder) % first time analyzing a set of data
    mkdir(day_folder);
else
    rmdir(day_folder, 's'); % replace existing folder
    mkdir(day_folder);
end

stitched_chunks = temperature_analyzer(raw_data.temp_raw, 0);
duration_percent_threshold = 0.15; % percentage threshold for adding to duration
reflected_light_threshold = 20000; % light level threshold, not much research has been done on how to set this threshold
time_before_impact = 2000; % how many ms to record before a peak linear acceleration
time_after_impact = 100; % how many ms to record after a peak linear acceleration

% text file w/ descriptions of each impact
fileID = fopen('impactlog.txt', 'w+'); 

i = time_before_impact + 1; % avoids MATLAB out-of-bound errors
count = 1;

while (i < length(raw_data.time)- (2*time_after_impact + 1)) % loops through the entire dataset
    k = i;
    if (raw_data.lin_acc_resultant(i) > LAT && raw_data.lin_acc_resultant(i) < 300) % potential impact, 2nd condition removes corrupted data
        peaklinacc = raw_data.lin_acc_resultant(i);
        duration = 0;
        k = i+1;
        upperbound = i+time_after_impact;
        while (k < upperbound && k < length(raw_data.time))
            upperbound = i+100;
            if (raw_data.lin_acc_resultant(k) > peaklinacc) % higher peak 
                peaklinacc = raw_data.lin_acc_resultant(k);
                i = k;
                duration = 0;
            elseif (raw_data.lin_acc_resultant(k) > duration_percent_threshold*peaklinacc) % part of impact
                duration = duration + 1;
            else
                break;
            end
            k = k+1;
        end
        j = i-1;
        while (j > i-time_before_impact && j > 1) % work backwards to start of impact
            if (raw_data.lin_acc_resultant(j) > duration_percent_threshold*peaklinacc) 
                duration = duration + 1;
            else
                break;
            end
            j = j-1;
        end
        
        % find maximum HIC within 140ms bounds
        % use HIC bounds t1 and t2 to estimate duration 
        HICstart = max([i-40,1]);
        HICend = min([i+100,length(raw_data.time)]);
        duration_HIC140 = 0;
        HIC = -1; 
        HICinteg = cumtrapz(raw_data.lin_acc_resultant(HICstart:HICend));
        
        for ih = 1:39
            for jh = ih+1:min([ih+36,140])
                tempHIC = (jh-ih)*(((jh-ih)^(-1) * (HICinteg(jh) - HICinteg(ih)))^2.5);
                if (tempHIC > HIC)
                    HIC = tempHIC;
                    duration_HIC140 = jh-ih;
                end
            end
        end
        
        % find maximum HIC within 100ms bounds typical of X-Patch
        % use HIC bounds t1 and t2 to estimate duration 
        HICstart = max([i-10,1]);
        HICend = min([i+90,length(raw_data.time)]);
        duration_HIC100 = 0;
        HIC = -1; 
        HICinteg = cumtrapz(raw_data.lin_acc_resultant(HICstart:HICend));
        
        for ih = 1:39
            for jh = ih+1:min([ih+36,100])
                tempHIC = (jh-ih)*(((jh-ih)^(-1) * (HICinteg(jh) - HICinteg(ih)))^2.5);
                if (tempHIC > HIC)
                    HIC = tempHIC;
                    duration_HIC100 = jh-ih;
                end
            end
        end
        
        % find maximum HIC within 40ms bounds typical of HIT system
        % use HIC bounds t1 and t2 to estimate duration 
        HICstart = max([i-8,1]);
        HICend = min([i+32,length(raw_data.time)]);
        duration_HIC40 = 0;
        HIC = -1; 
        HICinteg = cumtrapz(raw_data.lin_acc_resultant(HICstart:HICend));
        
        for ih = 1:39
            for jh = ih+1:min([ih+36,40])
                tempHIC = (jh-ih)*(((jh-ih)^(-1) * (HICinteg(jh) - HICinteg(ih)))^2.5);
                if (tempHIC > HIC)
                    HIC = tempHIC;
                    duration_HIC40 = jh-ih;
                end
            end
        end        
        
        tempIn = tempcheck(i, stitched_chunks);
        if (temp_verif == 1) 
            if (tempIn)
                temp_string = "suggests in ear";
                temp = 1;
            else
                temp_string = "suggests not in ear";
                temp = 0;
            end
        else
            temp_string = "not checked";
            temp = -1;
        end
        
        lightIn = false;
        if (raw_data.light_ref(i) <= reflected_light_threshold) % meets light threshold
            lightIn = true;
            j = j+1;
            while (j < k) % check whether it meets threshold for long enough
                if (raw_data.light_ref(j) > reflected_light_threshold)
                    lightIn = false;
                    break;
                end
                j = j+1;
            end
        end
        
        if (light_verif == 1)
            if (lightIn)
                light_string = "suggests in ear";
                light = 1;
            else
                light_string = "suggests not in ear";
                light = 0;
            end
        else
            light_string = "not checked";
            light = -1;
        end
        
        if (duration >= DT || duration_HIC140 >= DT || duration_HIC100 >= DT || duration_HIC40 >= DT) % meets duration threshold
            impact_time_ms = i;
            % prints info to console
            fprintf('%d: Impact at %d ms, Peak linacc = %0.1f g, Duration = %d ms, Temperature %s, Light %s\n', ...
            count, impact_time_ms,...
            peaklinacc, duration, temp_string, light_string); 
            
            % prints info to impact log
            fprintf(fileID, '%d: Impact at %d ms, Peak linacc = %0.1f g, Duration = %d ms, Temperature %s, Light %s\n', ...
                count, impact_time_ms,...
                peaklinacc, duration, temp_string, light_string);

            % saves info to excel file
            if count == 1
                writematrix("Impact Number", fileID_xls, 'Range', 'A1', 'Sheet', day);
                writematrix("Impact time (ms)", fileID_xls, 'Range', 'B1', 'Sheet', day);
                writematrix("Peak (g)", fileID_xls, 'Range', 'C1', 'Sheet', day);
                writematrix("Duration (ms)", fileID_xls, 'Range', 'D1', 'Sheet', day);
                writematrix("HIC Duration 140 (ms)", fileID_xls, 'Range', 'E1', 'Sheet', day);
                writematrix("HIC Duration 100 (ms)", fileID_xls, 'Range', 'F1', 'Sheet', day);
                writematrix("HIC Duration 40 (ms)", fileID_xls, 'Range', 'G1', 'Sheet', day);
                writematrix("Temperature", fileID_xls, 'Range', 'H1', 'Sheet', day);
                writematrix("Light", fileID_xls, 'Range', 'I1', 'Sheet', day);
            end
            writematrix(count, fileID_xls, 'Range', strcat('A', int2str(count+1)), 'Sheet', day);
            writematrix(impact_time_ms, ...
                fileID_xls, 'Range', strcat('B', int2str(count+1)), 'Sheet', day);
            writematrix(peaklinacc, fileID_xls, 'Range', strcat('C', int2str(count+1)), 'Sheet', day);
            writematrix(duration, fileID_xls, 'Range', strcat('D', int2str(count+1)), 'Sheet', day);
            writematrix(duration_HIC140, fileID_xls, 'Range', strcat('E', int2str(count+1)), 'Sheet', day);
            writematrix(duration_HIC100, fileID_xls, 'Range', strcat('F', int2str(count+1)), 'Sheet', day);
            writematrix(duration_HIC40, fileID_xls, 'Range', strcat('G', int2str(count+1)), 'Sheet', day);
            writematrix(temp, fileID_xls, 'Range', strcat('H', int2str(count+1)), 'Sheet', day);
            writematrix(light, fileID_xls, 'Range', strcat('I', int2str(count+1)), 'Sheet', day);

            % creates zoomed in figure
            curFig = figure('units','normalized','outerposition',[0 0 1 1],'visible','off');
            clf;
            graph2(1) = subplot(2,1,1);
            hold on;
            time_vals = transpose(linspace(-1 * time_before_impact, time_after_impact, time_before_impact + time_after_impact + 1));
            plot(time_vals, raw_data.lin_acc_resultant(i-time_before_impact:i+time_after_impact),...
                time_vals, raw_data.lin_acc_xyz(i-time_before_impact:i+time_after_impact,1),...
                time_vals, raw_data.lin_acc_xyz(i-time_before_impact:i+time_after_impact,2),...
                time_vals, raw_data.lin_acc_xyz(i-time_before_impact:i+time_after_impact,3));
            xlabel('\fontsize{16}Time (ms)');
            ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
            legend('Resultant','x dir','y dir','z dir');
            legend('Resultant','x dir','y dir','z dir');
            title('Linear Acceleration (x,y,z and resultant)');
            hold off 

            % Rotational Velocity
            graph2(2) = subplot(2,1,2);
            hold on;
            plot(time_vals, raw_data.rot_vel_resultant(i-time_before_impact:i+time_after_impact),...
                time_vals, raw_data.rot_vel_xyz(i-time_before_impact:i+time_after_impact,1),...
                time_vals, raw_data.rot_vel_xyz(i-time_before_impact:i+time_after_impact,2),...
                time_vals, raw_data.rot_vel_xyz(i-time_before_impact:i+time_after_impact,3));
            xlabel('\fontsize{16}Time (ms)');
            ylabel('\fontsize{16}Rotational Velocity (deg/s)');
            legend('Resultant','x dir','y dir','z dir');
            title('Rotational Velocity (x,y,z and resultant)');
            hold off
            fulltitle = pin + " " + day + ": Impact " + int2str(count) + ...
                " at " + int2str(impact_time_ms) + " ms";
            sgtitle(fulltitle,'Interpreter','none');

            linkaxes(graph2,'x')

            % saves figure and mat file to folder
            impfigname = strcat(pin, '_', day, '-', int2str(count), '.fig');
            impmatname = strcat(pin, '_', day, '-', int2str(count), '.mat');
            set(curFig, 'CreateFcn', 'set(gcf,"Visible","on")');
            saveas(curFig,impfigname,'fig');
            movefile(impfigname, day_folder);
            close(curFig);
            
            time_subset = raw_data.time(i-time_before_impact:i+time_after_impact);
            lin_acc_res_subset = raw_data.lin_acc_resultant(i-time_before_impact:i+time_after_impact);
            lin_acc_xyz_subset = raw_data.lin_acc_xyz(i-time_before_impact:i+time_after_impact,:);
            rot_vel_res_subset = raw_data.rot_vel_resultant(i-time_before_impact:i+time_after_impact);
            rot_vel_xyz_subset = raw_data.rot_vel_xyz(i-time_before_impact:i+time_after_impact,:);
            temp_subset = raw_data.temp_raw(i-time_before_impact:i+time_after_impact);
            amblight_subset = raw_data.light_amb(i-time_before_impact:i+time_after_impact);
            reflight_subset = raw_data.light_ref(i-time_before_impact:i+time_after_impact);

            % saves necessary variables to mat file for further
            % analysis
            save(impmatname, 'day', 'peaklinacc', 'time_subset', 'lin_acc_res_subset', ...
                'lin_acc_xyz_subset', 'rot_vel_res_subset', 'rot_vel_xyz_subset', ...
                'temp_subset', 'amblight_subset', 'reflight_subset', 'time_vals');
            movefile(impmatname, day_folder);
            
            % saves subsets of time, linear acceleration, and rotational
            % velocity to csv file for input to ML training set 
            fileID_csv = strcat(pin, '_', day, '-', int2str(count), '.csv');
%             csv_matrix = [cat(1,"Linear acceleration",lin_acc_res_subset), ...
%                 cat(1,"Rotational velocity",rot_vel_res_subset), ...
%                 cat(1,"Time values",time_vals'), ...
%                 cat(1,"Absolute time values",time_subset), ...
%                 cat(1,"Temperature",temp_subset), ...
%                 cat(1,"Ambient light",amblight_subset), ...
%                 cat(1,"Reflected light",reflight_subset)];
            csv_matrix = [["Linear acceleration" ; lin_acc_res_subset], ...
                ["Rotational velocity" ; rot_vel_res_subset], ...
                ["Time values" ; time_vals], ...
                ["Absolute time values" ; time_subset], ...
                ["Temperature" ; temp_subset], ...
                ["Ambient light" ; amblight_subset], ...
                ["Reflected light" ; reflight_subset]];
            writematrix(csv_matrix, fileID_csv);
            movefile(fileID_csv, day_folder);
            count = count + 1;
        end  
        k = k + time_after_impact;
    end
    i = k + 1; % avoids recording the same impact multiple times
end

fclose('all');
movefile('impactlog.txt', day_folder);
cd(impFolder); % return to pin folder


end


%% User input, folder detection/creation
function foldername = makefolder(pin, date)
    foldername = strcat(pin, '_', date);
    if ~isfolder(foldername) % first time analyzing a set of data
        mkdir(foldername);
    end
end

%% Convert to raw data
function raw_struct = convert_data(output)
    num_milsec_total = length(output) * length(output(1).data);
    raw_output = zeros(num_milsec_total,9);
    start_index = 1;
    end_index = length(output(1).data);
    for i = 1:length(output)
        raw_output(start_index:end_index,:) = output(i).data;
        start_index = end_index + 1;
        if i < length(output)
            end_index = start_index + length(output(i+1).data) - 1;
        end
    end

    %saves data in arrays easy to use for plotting
    raw_struct.lin_acc_xyz = raw_output(:,1:3); %units = g
    raw_struct.lin_acc_resultant = sqrt(raw_struct.lin_acc_xyz(:,1).^2 + ...
        raw_struct.lin_acc_xyz(:,2).^2 + raw_struct.lin_acc_xyz(:,3).^2);
    raw_struct.rot_vel_xyz = raw_output(:,4:6); %units = deg/s
    raw_struct.rot_vel_resultant = sqrt(raw_struct.rot_vel_xyz(:,1).^2 + ...
        raw_struct.rot_vel_xyz(:,2).^2 + raw_struct.rot_vel_xyz(:,3).^2);
    raw_struct.time = transpose(1:1:length(raw_output)); %units = ms
    raw_struct.temp_raw = raw_output(:,9);
    raw_struct.light_amb = raw_output(:,7);
    raw_struct.light_ref = raw_output(:,8);
end

%% Plot and save full figure
function plotfig(plot_data)
    curFig = figure('units','normalized','outerposition',[0 0 1 1]);
    clf
    graph1(1) = subplot(2,2,1);
    hold on;
    plot(plot_data.time, plot_data.lin_acc_resultant,...
        plot_data.time, plot_data.lin_acc_xyz(:,1),...
        plot_data.time, plot_data.lin_acc_xyz(:,2),...
        plot_data.time, plot_data.lin_acc_xyz(:,3));
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Linear Acceleration Left Earpiece (g)');
    legend('Resultant','x dir','y dir','z dir');
    title('\fontsize{24}Linear Acceleration (x,y,z and resultant)');
    hold off 

    % Rotational Velocity
    graph1(2) = subplot(2,2,2);
    hold on;
    plot(plot_data.time, plot_data.rot_vel_resultant,...
        plot_data.time, plot_data.rot_vel_xyz(:,1),...
        plot_data.time, plot_data.rot_vel_xyz(:,2),...
        plot_data.time, plot_data.rot_vel_xyz(:,3));
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Rotational Velocity (deg/s)');
    legend('Resultant','x dir','y dir','z dir');
    title('\fontsize{24}Rotational Velocity (x,y,z and resultant)')
    hold off

    % temperature
    graph1(3) = subplot(2,2,3);
    hold on;
    plot(plot_data.time, plot_data.temp_raw);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Temperature');
    title('\fontsize{24}Temperature');
    hold off

    % light
    graph1(4) = subplot(2,2,4);
    hold on;
    plot(plot_data.time, plot_data.light_amb, plot_data.time, plot_data.light_ref);
    xlabel('\fontsize{16}Time (ms)');
    ylabel('\fontsize{16}Light level');
    legend('Ambient light','Reflected light');
    title('\fontsize{24}Light');
    hold off

    linkaxes(graph1,'x') 
end

%% Use temperature analyzer to determine if temperature suggests in ear
function inRange = tempcheck(time_in, stitched_chunks)
    inRange = false; 
    for i = 1:length(stitched_chunks)
        if (time_in-5 >= stitched_chunks{i}(1) && time_in+5 <= stitched_chunks{i}(2))
            inRange = true;
            break
        end
    end

end

end
