function DASHR_Converter(varargin)

% add paths to tools
mfilepath = fileparts(which('DASHR_Converter.m'));
addpath(genpath(fullfile(mfilepath)));
addpath(genpath(fullfile(mfilepath,'../tools')));
addpath(genpath(fullfile(mfilepath,'../display')));

% For the first run of DASHR_Converter, program will launch the GUI to get
% options. Upon clicking Launch, the GUI will recall this function and pass
% the proper parameter struct. Struct contains 5 fields:
%           type - flag specifying if data to be loaded is DASHR or HDAM
%           generate_files - flag to output files or not
%           output_format - Format to output files in
%           workspace - Flag to push output to global workspace
%           file_selection - Type of file selection (recursive or direct)
% For running without the GUI care must be taken as no input validation is
% carried out. 
%       - convert and output_workspace cannot both be true
% Example parameter set:
%   params.type = 'DASHR';
%   params.generate_files = false;
%   params.output_format = 'MATLAB';
%   params.workspace = true;
%   params.file_selection = 'Direct'
if isempty(varargin)
    % run GUI and return
    if verLessThan('Matlab','9.0')
        disp('Please update MATLAB to 2016a or newer for full functionality');
    else
        BinaryConverter();
    end
    return
else
    conversionParameters = varargin{1};
end

type = conversionParameters.type;
convert = conversionParameters.generate_files;
convertedFormat = conversionParameters.output_format;
output_workspace = conversionParameters.workspace;
selectionType = conversionParameters.file_selection; 

if ~convert && ~output_workspace
    disp('No output generation selected');
    return;
end

output = [];
metadata = [];

switch selectionType
    case 'Recursive'
        lastFolder = getFolderSelection();
        if(lastFolder == 0)
            disp('No folder selected');
            return
        end    
        files = rdir(fullfile(lastFolder, '**','*.BIN'));
    case 'Direct'
       files = getFileSelection();
    case 'Cancel'
        return;
end

[output, metadata] = convertFiles(files, type, convert, convertedFormat);

switch output_workspace
    case true
        if ~isempty(output) 
            % sort by trigger time before output
            output = sortOutputByTriggerTime(output, metadata);
            
            putvar(output);
            putvar(metadata);
        end
    case false
        
    otherwise
        
end
end


function lastFolder = getFolderSelection()
    lastFolder = loadConfigLastFolder();
    lastFolder = uigetdir(lastFolder, 'Choose Starting Directory');
    saveConfigLastFolder(lastFolder);
end

function files = getFileSelection()
    lastFolder = loadConfigLastFolder();
    
    [files, path] = uigetfile({'*.BIN';'*.CSV'},'Multiselect','on','Choose DASHR files',lastFolder);
    lastFolder = path;
    
    
    if files ~= 0
        files = fullfile(path, files);
        
        % single file selection for uigetfile returns a char array, so convert it
        % to a cell for easier processing later
        if ~iscell(files)
            files = {files};
        end
        
        files = cell2struct(files, 'name', 1);
    else
        files = struct;
    end
    
    if lastFolder ~= 0
        saveConfigLastFolder(lastFolder);
    end
end

function lastFolder = loadConfigLastFolder()
    config_folder = fileparts(which('DASHR_Converter.m'));
    lastFolder = '';
    % Load the configuration file, if it exists
    if exist([config_folder,'\config.mat'], 'file')
        try
            load([config_folder,'\config.mat'], 'lastFolder')
        catch
            disp('Error loading configuration file');
        end
    else
        % set up default variables
        lastFolder = pwd;
    end
end

function saveConfigLastFolder(lastFolder)
    config_folder = fileparts(which('DASHR_Converter.m'));
    try
        if lastFolder ~= 0
            save([config_folder,'\config.mat'],'lastFolder');
        end
    catch
        disp('Error saving configuration file');
    end
end

function [output, metadata] = convertFiles(files, data_type, convert, convert_format)
    % only skip over files if we're converting them directly and not
    % outputting to workspace
    skipped_files = [];
    if convert
        [files skipped_files] = filterConvertedFiles(files, convert_format);
    end
    
    output = {};
    metadata = [];
    prev_meta = [];
    corrupted = 0;
    print = 0;
    
    h = waitbar(0,'Converting');
    for i = 1:length(files)
        if isempty(fieldnames(files))
            disp('No files to convert');
            delete(h)
            return
        end
        
        [path, name, ext] = fileparts(files(i).name);
        waitbar(i/length(files),h,['Converting ', name, ext]);
        
        if convert
            [out, meta] = bin_reader(path, [name ext], data_type);
            file_writer(meta, out, fullfile(path, 'Converted'), data_type, convert_format);
        else
            if corrupted == 1
                if i == 1
                    [out, meta] = bin_reader(path, [name ext], data_type, 0, [], print);
                    output = [output, out];
                    metadata = [metadata, meta];
                    prev_meta = meta;
                else
                    if i == 10
                        print = 0;
                    else
                        print = 0;
                    end
                    prev_meta.triggerTime = prev_meta.triggerTime + 1;
                    [out, meta] = bin_reader(path, [name ext], data_type, 1, prev_meta, print);
                    output = [output, out]; % cell array, because we need to join it to metadata later and sort it
                    metadata = [metadata, meta];
                end
            else
                [out, meta] = bin_reader(path, [name ext], data_type, 0, [], print);
                output = [output, out];
                metadata = [metadata, meta];
            end
        end
    end
    fprintf(['Conversion finished:\rConverted: ',num2str(length(files)),'\rSkipped: ',num2str(length(skipped_files)),'\r']);
    delete(h)
end

function output_sorted = sortOutputByTriggerTime(data, metadata)

    % join data with metadata for easier sorting, to keep everything
    % together
    data_joined = join_meta_output(data, metadata);
    [~,sorted_index] = sort([data_joined.triggerTime]);
    data_sorted = data_joined(sorted_index);
    
    output_sorted = data_sorted;   
end

function output_struct = join_meta_output(data, metadata)
    data_struct = cell2struct(data,'data')';
    names = [fieldnames(metadata); fieldnames(data_struct)];
    output_struct = cell2struct([struct2cell(metadata); struct2cell(data_struct)], names, 1); 
end

function [meta, output] = separate_meta_output(data_in)
    output = vertcat(data_in.data);
    meta = rmfield(meta, 'data');
end


function [filtered_files skipped_files] = filterConvertedFiles(files, convert_format)
    filtered_files = struct;
    skipped_files = struct;
    convert_ext = '';
    
    if strcmp(convert_format, 'MATLAB')
        convert_ext = '.mat';
    elseif strcmp(convert_format, 'CSV')
        convert_ext = '.CSV';
    else
        disp('Unsupported extension, aborting');
        return;
    end
    
    
    h = waitbar(0,'Searching for already converted files...');
    
    for i = 1:length(files)
        try
            % break down actual data file path into parts
            [path, name, ext] = fileparts(files(i).name);
            
            if mod(i, 10) == 0
                % matlab waitbar treats single '\' as a latex command,
                % requires all single backslash to be replaced with '\\'
                waitbar(i/length(files),h, regexprep({sprintf('Checking %s\\%s%s',path, name, ext)}, '\\', '\\\\'));
            end
            
            % add in the \Converted\ folder structure
            converted_file_name = [fullfile(path, 'Converted',name), convert_ext];
            
            if ~exist(converted_file_name, 'file')
                if isempty(fieldnames(filtered_files))
                    filtered_files = files(i);
                else
                    filtered_files(end + 1) = files(i);
                end
            else
                if isempty(fieldnames(skipped_files))
                    skipped_files = files(i);
                else
                    skipped_files(end + 1) = files(i);
                end
            end
        catch ME
            disp('Error searching for already converted files:');
            disp(['File: ',fullfile(path,name, ext)]);
            delete(h);
            disp(ME);
        end        
    end
    delete(h);
    
end



