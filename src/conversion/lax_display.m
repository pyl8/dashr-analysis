clear all
close all

% Message for instructions
h = msgbox('If using this code quickly evaluate drop Data make sure of the following: Under Data Input, both DASHR(9CH) and Recursive are selected, Under Data Output "Load into Workspace" is switched to teh On position, When done entiring data into workspace press any key to continue');
pause(2)

% Runs DASHR Converter app
DASHR_Converter()

pause()
% parentFolder = 'Z:\td106\D6\DASHR\Left_Ear\Helmeted\Frontal\DEMEANED\D6\FILT_CFC1000';
% watch_Folder = ([parentFol der ]);
%puts the data in one large array titled Raw_data
% col 1-3 = x,y,z linear acceleration, col4-6 = x,y,z angular velocity
% col 7 = light data, col 8 = pulse data, col 9 = temp data

num_milsec_total = length(output) * length(output(1).data);
raw_data = zeros(num_milsec_total,9);
start_index = 1;
end_index = length(output(1).data);
for i = 1:length(output)
    raw_data(start_index:end_index,:) = output(i).data;
    start_index = end_index + 1;
    if i < length(output)
        end_index = start_index + length(output(i+1).data) - 1;
    end
end

%saves data in arrays easy to use for plotting
lin_acc_xyz = raw_data(:,1:3); %units = g
lin_acc_resultant = sqrt(lin_acc_xyz(:,1).^2 + lin_acc_xyz(:,2).^2 + lin_acc_xyz(:,3).^2);
rot_vel_xyz = raw_data(:,4:6); %units = deg/s
rot_vel_resultant = sqrt(rot_vel_xyz(:,1).^2 + rot_vel_xyz(:,2).^2 + rot_vel_xyz(:,3).^2);
time = [1:1:length(raw_data)]; %units = ms

%Temporary Addition
findtime=(abs(time-2.53*10^5));
[t1,t2]=min(findtime)
DASHRresult=lin_acc_resultant(200000:t2);
[maxDASHRresult,maxDASHRloc]=max(DASHRresult)

%Plotting Data
%Linear Acceleration
figure('units','normalized','outerposition',[0 0 1 1]); %why did I do this?
graph1(1) = subplot(2,1,1);
hold on;
plot(time, lin_acc_resultant, time,lin_acc_xyz(:,1),time, lin_acc_xyz(:,2),time,lin_acc_xyz(:,3));
xlabel('Time (ms)');
ylabel('Linear Acceleration Left Earpiece (g)');
legend('Resultant','x dir','y dir','z dir');
title(['\fontsize{24}Linear Acceleration (x,y,z and resultant)']);
hold off 
% Rotational Velocity
graph1(2) = subplot(2,1,2);
hold on;
plot(time, rot_vel_resultant, time,rot_vel_xyz(:,1),time,rot_vel_xyz(:,2),time,rot_vel_xyz(:,3));
xlabel('Time (ms)');
ylabel('Rotational Velocity (deg/s)');
legend('Resultant','x dir','y dir','z dir');
title('Rotational Velocity (x,y,z and resultant)')
hold off

linkaxes([graph1],'x') %links the x axis for both plots, if not needed then comment this line out 
