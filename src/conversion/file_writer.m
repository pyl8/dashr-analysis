function status = file_writer(metadata, output_data, output_path, data_type, file_type)
    if isempty(metadata)
        status = false;
        return;
    end

    if strcmp(file_type, 'CSV')
        status = write_csv(metadata, output_data, output_path, data_type);
    elseif strcmp(file_type, 'MATLAB')
        status = write_m(metadata, output_data, output_path, data_type);
    else
        disp('Invalid file type specified');
        status = false; 
        return;
    end
end

function status = write_csv(metadata, output_data, output_path, data_type)
    [~, file, ~] = fileparts(metadata.filename);
    
    if exist(fullfile(output_path, [file '.CSV']), 'file')
        status = false;
        disp(['File already exists, skipping .CSV output: ', output_path, '\', file, '.CSV']);
        return;
    end
   
    try
        if ~exist(fullfile(output_path),'dir')
            mkdir(fullfile(output_path))
        end
        
        fid = fopen(fullfile(output_path, [file '.CSV']),'w+');
    catch ME
        disp(['Unable to open file ', output_path, '\', file, '.CSV']);
        disp(ME);
        status = false;
        return;
    end
    
    try
        % create and write header
        fprintf(fid, create_header_string(metadata));
        
        % create and write column names after header
        fprintf(fid, create_column_name_string(data_type));
        
        % create data format string and write output data
        fprintf(fid,create_data_format_string(metadata),output_data');
        
        fclose(fid);
    catch ME
        disp(['Unable to write file data ', output_path, '\', file, '.CSV']);
        disp(ME);
        status = false;
        return;
    end
    
    status = true;
    return;
end

function status = write_m(metadata, output_data, output_path, data_type)
    [~, file, ~] = fileparts(metadata.filename);

    if exist(fullfile(output_path, [file '.mat']), 'file')
        status = false;
        disp(['File already exists, skipping .mat output: ', fullfile(output_path, file), '.m']);
        return;
    end

    try
        if ~exist(fullfile(output_path),'dir')
            mkdir(fullfile(output_path))
        end
    catch ME
        disp(['Unable to create directory for file ', fullfile(output_path, file), '.mat']);
        disp(ME);
        status = false;
        return;
    end
    
    data = output_data;
    save(fullfile(output_path, [file '.mat']), 'data','metadata', 'data_type');

    status = true;
    return;
end


function header_string = create_header_string(metadata)

try
    header_string = ['Filename,',metadata.filename,',\r\n',...
        'Trigger Time,',num2str(metadata.triggerTime),',\r\n',...
        'EST Trigger Time,',datestr(datenum([1970 1 1 0 0 double(metadata.triggerTime - 4*60*60)])),',\r\n',...
        'Serial Number,',num2str(metadata.serialNum),',\r\n',...
        'Sample Frequency,',num2str(metadata.sampleFrequency),',\r\n',...
        'Pin Count,',num2str(metadata.pinCount),',\r\n',...
        'Trigger Index,',num2str(metadata.triggerIndex),',\r\n',...
        'Timesteps Per Trigger,',num2str(metadata.timestepsPerTrigger),',\r\n',...
        'File Block Count,',num2str(metadata.fileBlockCount),',\r\n',...
        'Timesteps Per Block,',num2str(metadata.timestepsPerBlock),',\r\n',...
        'Sample Frequency,',num2str(metadata.sampleFrequency),',\r\n'];
catch ME
    disp('Error creating header string');
    disp(ME);
    header_string = '';
end

end

function column_string = create_column_name_string(data_type)
    if strcmp(data_type, 'DASHR')
        column_string = ['Ax (g),'...
            'Ay (g),'...
            'Az (g),'...
            'Wx (deg/s),'...
            'Wy (deg/s),'...
            'Wz (deg/s),'...
            'Pulse,'...
            'Ambient,'...
            'Temp,\r\n'];
    elseif strcmp(data_type, 'HDAM')
        column_string = ['HR Ax (g),'...
            'HR Ay (g),'...
            'HR Az (g),'...
            'LR Ax (g),'...
            'LR Ay (g),'...
            'LR Az (g),'...
            'Wx (deg/s),'...
            'Wy (deg/s),'...
            'Wz (deg/s),'...
            'Pulse,'...
            'Ambient,'...
            'Temp,\r\n'];
    else
        column_string = '';
        disp('Type unrecognized, no column names available');
    end  
end

function data_format_string = create_data_format_string(metadata)
    % variable length string containing '%f,%f,...,%f,'
    data_format_string = '';
    for i=1:metadata.pinCount
        data_format_string = strcat(data_format_string, {'%f,'});
    end
    data_format_string = strcat(data_format_string,'\r\n');
    data_format_string = char(data_format_string);
end
