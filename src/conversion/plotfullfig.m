close all
DASHR_Converter()
pause()

%% Convert to raw data
num_milsec_total = length(output) * length(output(1).data);
raw_data = zeros(num_milsec_total,9);
start_index = 1;
end_index = length(output(1).data);
for i = 1:length(output)
    raw_data(start_index:end_index,:) = output(i).data;
    start_index = end_index + 1;
    if i < length(output)
        end_index = start_index + length(output(i+1).data) - 1;
    end
end
%saves data in arrays easy to use for plotting
lin_acc_xyz = raw_data(:,1:3); %units = g
lin_acc_resultant = sqrt(lin_acc_xyz(:,1).^2 + lin_acc_xyz(:,2).^2 + lin_acc_xyz(:,3).^2);
rot_vel_xyz = raw_data(:,4:6); %units = deg/s
rot_vel_resultant = sqrt(rot_vel_xyz(:,1).^2 + rot_vel_xyz(:,2).^2 + rot_vel_xyz(:,3).^2);
time = 1:1:length(raw_data); %units = ms
temp_raw = raw_data(:,9);
light_amb = raw_data(:,7);
light_ref = raw_data(:,8);

curFig = figure('units','normalized','outerposition',[0 0 1 1]); %why did I do this?
clf
graph1(1) = subplot(2,2,1);
hold on;
plot(time, lin_acc_resultant,...
    time, lin_acc_xyz(:,1),...
    time, lin_acc_xyz(:,2),...
    time, lin_acc_xyz(:,3));
xlabel('\fontsize{10}Time (ms)');
ylabel('\fontsize{10}Linear Acceleration Left Earpiece (g)');
legend('Resultant','x dir','y dir','z dir');
title('\fontsize{16}Linear Acceleration (x,y,z and resultant)');
hold off

% Rotational Velocity
graph1(2) = subplot(2,2,2);
hold on;
plot(time, rot_vel_resultant,...
    time, rot_vel_xyz(:,1),...
    time, rot_vel_xyz(:,2),...
    time, rot_vel_xyz(:,3));
xlabel('\fontsize{10}Time (ms)');
ylabel('\fontsize{10}Rotational Velocity (deg/s)');
legend('Resultant','x dir','y dir','z dir');
title('\fontsize{16}Rotational Velocity (x,y,z and resultant)')
hold off

% temperature
graph1(3) = subplot(2,2,3);
hold on;
plot(time, temp_raw);
xlabel('\fontsize{10}Time (ms)');
ylabel('\fontsize{10}Temperature');
title('\fontsize{16}Temperature');
%stitched_chunks = temperature_analyzer(temp_raw); % outputs stitched_chunks to workspace
hold off

% light
graph1(4) = subplot(2,2,4);
hold on;
plot(time, light_amb, time, light_ref);
xlabel('\fontsize{10}Time (ms)');
ylabel('\fontsize{10}Light level');
legend('Ambient light','Reflected light');
title('\fontsize{16}Light');
hold off

linkaxes(graph1,'x') 