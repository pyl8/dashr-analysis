function status = sqlite_writer()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

mfilepath = fileparts(which('sqlite_writer.m'));
db_path = fullfile(mfilepath,'..','tools','sqlite','data.sqlite3');


% make sure database exists, otherwise create it
% also check to see if driver path is present
if ~exist(db_path, 'file')
    install_jdbc_drivers();
    create_sqlite_db(db_path);
end

% connect to database, no username/password required
conn = database(db_path, [], [],'org.sqlite.JDBC',['jdbc:sqlite:',db_path]);

% write to database


% close database
close(conn);


end

function status = write_sqlite(metadata, output_data, db_conn)
    
end

function status = sqlite_createtable(conn, tablename, columnnames, datatypes, constraints)

end

function status = sqlite_createcolumn(conn, tablename, columnname, datatype, constraints)

end

