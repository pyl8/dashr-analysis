%   bin_reader(path, filename, pathout)
% Converts binary files to output CSV
% INPUT:
%   path - Input path leading to file
%   filename - Filename to convert
%   pathout - Path to place output file, do not make it the same as input
%   type - Specifies what generated the data, important for cal factor
%       Available types are 'HDAM', 'DASHR'
%       HDAM - 12 channels, with high and low range accel, gyro, pulse
%       DASHR - 9 channels, with high range accel, gyro, pulse

function [output, metadata] = bin_reader(path, filename, type, use_prev, prev_meta, print)

% when using the automated MVTrak downloader, sometimes the file
% creation/modified time will be incorrect. 
% mv1_modified_date_restorer pulls the correct RTC time from the header and
% sets the OS last modified date field. 
%mv1_modified_date_restorer(fullfile(path,filename));

blocknum = 1;
count = 512;
metadata = [];
num_skipped = 0;

if use_prev ~= 1
    prev_meta = [];
end

current_row = 1;

try
    fid = fopen(fullfile(path,filename));
    
    while(count == 512)
        
        % read a 512 byte block
        [data, count] = fread(fid, 512,'uint8=>uint8');
        
        % if it's an empty block abort, otherwise inc blocknum and add to real
        % data
        if ~isempty(data)
            if print == 1 && blocknum >= 1
                print_data(data);
            end
            if count == 512 && blocknum == 1 && data(1) == 99 && use_prev ~= 1
                % metadata block always resides in the first 512 bytes of
                % the file, and the first entry is always '99'
                metadata = process_header(data);
                metadata.filename = filename;
                metadata.data_type = type;
                
%                 fprintf('Time steps per block: %d\n', metadata.timestepsPerBlock);
%                 fprintf('Pin count: %d\n', metadata.pinCount);
%                 fprintf('Trigger time: %d\n', metadata.triggerTime);
                % initialize the output array, preallocated for speed.
                out = zeros(metadata.timestepsPerTrigger, metadata.pinCount, 'uint16');
                
                %fprintf('Block %d Trigger Time: %d\n', blocknum, metadata.triggerTime);
                
                blocknum = blocknum + 1;
            elseif count == 512 && blocknum == 1 && use_prev == 1
                metadata = prev_meta;
                metadata.filename = filename;
                metadata.data_type = type;
                
                %out = zeros(metadata.timestepsPerTrigger, metadata.pinCount, 'uint16');
                temp = process_block(data, metadata.pinCount);
                out = reshape(temp, metadata.pinCount, metadata.timestepsPerBlock)';
                
                %fprintf('Block %d Trigger Time: %d\n', blocknum, metadata.triggerTime);
                
                blocknum = blocknum + 1;
            elseif count == 512 && blocknum == 1 && data(1) ~= 99
                % unit was able to create the file but shut off before writing
                % any metadata
                % if this occurs, we can't do any further processing, so
                % abort. 
                disp(['No metadata for file, aborting: ',fullfile(path,filename)]);
                fclose(fid);
                output = []; 
                return;
            elseif count == 512 && blocknum <= (metadata.fileBlockCount + 1) && data(1) ~= 0 && use_prev == 0
                temp3 = process_block(data, metadata.pinCount);
                
                % broken down into channels
                out(current_row:(current_row + (metadata.timestepsPerBlock-1)), :) = reshape(temp3, metadata.pinCount, metadata.timestepsPerBlock)';
                %out(current_row:(current_row + 25), :) = reshape(temp3, 26, metadata.pinCount);
                current_row = current_row + metadata.timestepsPerBlock;
                
                blocknum = blocknum + 1;
            elseif count == 512 && blocknum <= (metadata.fileBlockCount + 1) && data(1) ~= 0 && use_prev == 1
                temp3 = process_block(data, metadata.pinCount);
                %temp_add = reshape(temp3, metadata.pinCount, metadata.timestepsPerBlock)';
                %fprintf('dimensions of out: %d x %d; dimensions of temp_add: %d x %d', 
                out = [out; reshape(temp3, metadata.pinCount, metadata.timestepsPerBlock)'];
            elseif data(1) == 0
                num_skipped = num_skipped + 1;
            else
                count = 0;
            end
        end
    end
catch ME
    disp('Error loading/processing data for file:');
    disp(['File: ',path,filename]);
    disp(ME);
    return;
end
%fprintf('Number of blocks skipped: %d\n', num_skipped);
fclose(fid);
clear blocknum count fid temp3 data;

try
    output = applyCalibration(out, type);
catch ME
    disp(['Error applying calibration for file: ',path, '\',filename]);
    return;
end

clear out;

end

function out = applyCalibration(in, type)
    if strcmp(type, 'DASHR')
        out = double(calibrationDASHR(in));
    elseif strcmp(type, 'HDAM')
        out = double(calibrationHDAM(in));
    else
        out = in;
        disp('Type unrecognized, no calibration available');
    end       
end

function out = accel_cal(in, full_scale_range)
    out = double((double(in)-32768)*(double(full_scale_range)/32768));
end

function out = gyro_cal(in, full_scale_range)
    out = (double(in)-32768)*(full_scale_range/32768);
end

function dashr_out = calibrationDASHR(in)
    dashr_out = [accel_cal(in(:,1:3),200), gyro_cal(in(:,4:6),4000), double(in(:,7:8)), double(temp_cal(in(:,9), 321.4, 21))];
end

function hdam_out = calibrationHDAM(in)
    hdam_out = [accel_cal(in(:,1:3),200), accel_cal(in(:,4:6),32), gyro_cal(in(:,7:9),4000) double(in(:,10:12))];
end

function temp_out = temp_cal(temp_in, sensitivity, room_temp_offset)
    % sensitivity should be in LSB/degC
    % room temp offset should be in degC at 0 LSB (e.g. when sensor reads
    % 0, what is the true temperature in degC)
    temp_out = room_temp_offset + double(typecast(temp_in,'int16'))/sensitivity;
end

function data_cast = process_block(data, pin_count)
    milli_time = typecast(data(9:12),'uint32');
    data_cast = typecast(data(13:end-mod(500, pin_count*2)),'uint16');
end

function metadata = process_header(data)
    metadata.count = typecast(data(1:4),'uint32');
    metadata.sampleFrequency = typecast(data(5:8),'int32');
    metadata.adcResolution = typecast(data(9:12), 'uint32');
    metadata.pinCount = typecast(data(13:16), 'uint32');
    metadata.triggerIndex = typecast(data(17:20), 'uint32');
    metadata.timestepsPerTrigger = typecast(data(21:24), 'uint32');
    metadata.pretriggerBlockCount = data(25);
    metadata.fileBlockCount = typecast(data(29:32), 'uint32');
    metadata.timestepsPerBlock = typecast(data(33:36), 'uint32');
    metadata.bufferBlockCount = data(37);
    metadata.hardwareAveragingCount = data(38);
    metadata.triggerTime = typecast(data(41:44), 'int32');
    metadata.triggerSigma = data(45);
    metadata.numDigitalChannels = data(46);
    
    metadata.pinNumber = typecast(data(47:70), 'uint8');
    metadata.pinAvg = typecast(data(71:118), 'uint16');
    metadata.pinStdDev = typecast(data(121:216),'single');
    metadata.pinTriggerLevel = typecast(data(217:312), 'uint32');
    metadata.pinCalFactor = typecast(data(313:408),'single');
    
    metadata.MAC = data(409:414);
    %disp(dec2hex(MAC));
    
    metadata.serialNum = typecast(data(417:420), 'uint32');
    
    metadata.hw_rev = typecast(data(421:422), 'uint16');
    metadata.fw_rev = typecast(data(423:424), 'uint16');
    
    % 423:424 ax bias
    % 425:426 ay bias
    % 427:428 az bias
    metadata.accel_bias = typecast(data(425:430), 'int16');
    
    % 431:432 cal 1
    % 433:434 cal 2
    % 435:436 cal 3
    % 437:438 cal 4
    % 439:440 cal 5
    % 441:442 cal 6
    metadata.cal_factors = typecast(data(431:442), 'uint16');
    
    metadata.last_cal_time = typecast(data(445:448), 'uint32');
    % data(421:end) should be ignored. 
end





