# DASHR Analysis

## Description
MATLAB code used to process and iterate through DASHR data

## How to use 
If you are trying to extract potential impact data from a DASHR dataset, run **dashr_analysis_pin.m** in src/conversion. The script will open a GUI. For the PIN, enter any four-character String. For "plot full figures," select yes if you would like to see a plot of each full dataset; these plots are not saved. Next, enter the thresholds you would like to apply (linear acceleration, duration, temp/light checks for whether the DASHR is in the ear). Finally, click "Begin Analysis" and you will be prompted to select a folder. **Select the PIN folder that contains subfolders for each dataset.** For example, if you want to analyze data for PIN 3580, select the folder named "3580" with subfolders named "2018-01-01," "2018-01-02," etc. If you only have one dataset to analyze, you must still place that dataset folder in a PIN folder for the script to run properly. The script will run through each dataset and perform the following tasks:
1. Convert the .bin files to raw data arrays. If this is your first time analyzing a dataset, the script will save the raw data in a .mat file that can be loaded in the future, avoiding redundant conversions.
2. Print out potential head impact events to the console in MATLAB and a file named impactlog.txt for each dataset. Information printed includes the time in ms, peak linear acceleration, duration, and whether the temperature/light indicate that the DASHR is in the ear.
3. Create a folder in src/conversion with the PIN and thresholds. Inside this folder are subfolders for each dataset. Each subfolder contains a .fig, .mat, and .csv file for each potential impact. 
4. Create an Excel file with **each dataset as a separate sheet**, which stores important information for each potential head impact event.

**Note: there are multiple algorithms that calculate duration in the script. If any of them calculate a duration that meets the threshold, the event will be labeled a potential head impact. However, you can filter based on one of the algorithms by going to the Excel file and sorting by the appropriate column.**

Event .mat files are 2100ms worth of data - 2000ms prior to the peak linear acceleration and 100ms after. 
